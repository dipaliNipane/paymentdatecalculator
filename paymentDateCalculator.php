<?php

class PaymentDateCalculator {
    
    public function __construct($year) {
       $this->year = $year;
    }

    /**
     * Function to find given months date on which salary is to be paid
     * @param integer $month
     * @return salary date
     */
    public function findSalaryDate($month)
    {
        // Find last date of given month
        $lastDay = date('t-m-Y',strtotime('01-'.$month.'-'.$this->year));
        $dayNumber = date('N', strtotime($lastDay));
        // Check if last day of month is weekday
        if ((7- $dayNumber) > 1) {
            return $lastDay;
        }
        // As last day is week end find last weekday as salary day.
        $diffrence = ($dayNumber = 7 ? 2 : 1);
        $date = new DateTime($lastDay);
        $date->sub(new DateInterval('P' . $diffrence . 'D'));
        
        return $date->format('d-m-Y');
    }

    /**
     * Function to find given months bonus giving date
     * @param integer $month
     * @return string
     */
    public function findBonusDate($month) 
    {
        // 15th of given month
        $bonusDate = '15-' . $month . '-' . $this->year;
        $dayNumber = date('N', strtotime($bonusDate));
        // Check if 15th of month is weekday
        if ((7- $dayNumber) > 1) {
            return $bonusDate;
        }
        // As 15th  is weekend find next wednesday as bonus giving day.
        $diffrence = ($dayNumber = 7 ? 3 : 4);
        $date = new DateTime($bonusDate);
        $date->add(new DateInterval('P' . $diffrence . 'D'));
        
        return $date->format('d-m-Y');
    }

    /**
     * Function to process complete year to find salary dates and bonus dates.
     * @return array
     */
    public function processYear() 
    {
        $result = [];
        $months = array('January', 'February', 'March', 'April', 'May', 'June', 'July ', 'August', 'September', 'October', 'November', 'December');
        foreach($months as $key => $month) {
            $result[$key][0] = $month; 
            $result[$key][1] = $this->findSalaryDate(1+$key);
            $result[$key][2] = $this->findBonusDate(1+$key);
        }
        return $result;
    }

    /**
     * Function to generate csv containing salary date and bonus dates information of particular year
     */
    public function generateCsv() 
    {

        $file = fopen("paymentDates_".$this->year.".csv", "w");
        fputcsv($file, array ('Month', 'Salary date', 'Bonus date'));
        $list = $this->processYear();
        foreach ($list as $line) {
            fputcsv($file, $line);
        }

        fclose($file);
    }

}
// If user doesn't enters any specific year, we assume current year as expected year and process it.
$expectedYear = (isset($argv[1])? $argv[1] : date('Y'));
$paymentDatesCsv = new PaymentDateCalculator($expectedYear);
echo 'Output is : paymentDates_' . $expectedYear . '.csv' . PHP_EOL;
$paymentDatesCsv->generateCsv();

?>
